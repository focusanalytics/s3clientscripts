import argparse
import boto
import datetime
import fcntl
import fnmatch
import logging as LOG
import json

import os
import time
import socket
import struct
import traceback

from boto.s3.key import Key

LOG.basicConfig(format='%(asctime)s  %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='casUploadScript.log', filemode='a', level=LOG.INFO)

def stringToDate(string):
    """
    Given string of format "20170616" convert it to datetime object
    """
    return datetime.datetime.strptime(string, "%Y%m%d")

def compareDate(filePath):
    """
    Take filepath and test if the date is greater than current date
    """
    currentDatetime = datetime.datetime.now()
    fileDatetime    = stringToDate(filePath.split("/")[5])
    return fileDatetime < currentDatetime

def getPrivateIP(interfaceName="eth0"):
    """
    Get private IP for this box
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', interfaceName[:15])
    )[20:24])

def s3Path(filePath, bucketName, pathType):
    """
    return s3 path given filePath according to pathType (TRACKDATE | PREDICTIONDATE)
    """
    pathSplit      = filePath.split("/")
    processDate    = pathSplit[5]
    clientCode     = pathSplit[6]
    predictionType = pathSplit[7]
    trackDate      = pathSplit[8]
    fileName       = pathSplit[9]
    epochTime      = int(time.time())
    # For eth0 interface
    privateIP      = getPrivateIP("eth0")
    uploadFileName = "%s_%s_%s"%(str(epochTime), privateIP, fileName)
    if pathType == "TRACKDATE":
        return "%s/%s/%s/%s/%s"%(bucketName, predictionType, clientCode, trackDate, uploadFileName)
    elif pathType == "PREDICTIONDATE":
        return "%s/%s/%s/%s/%s"%(bucketName, predictionType, clientCode, processDate, uploadFileName)


def deleteFiles(filePath):
    """
    Delete files
    """
    try:
        os.remove(filePath)
        return True
    except:
        return False

def safeUpload(bucketKey, filePath, LOG, deleteFilesToo=False):
    """
    safe upload filePath
    """
    try:
        uploadStatus = bucketKey.set_contents_from_filename(filePath)
        # If uploadStatus is >= 0 byte
        if uploadStatus or uploadStatus == 0:
            LOG.info("Uploaded successfully: %s"%filePath)

            # Boolean check | Withold deleting files if they will be going to used by other
            if not deleteFilesToo:
                return True

            if deleteFiles(filePath):
                LOG.info("Deleted successfully: %s"%filePath)
                return True

        return False
    except:
        LOG.error("Failed uploading:\n%s"%traceback.format_exc())
        return False


def removeEmptyDirs(CAS_FILES):
    """
    Given filePath contains .csv check if the directory is empty and remove it
    """
    try:
        os.popen("find %s -empty -type d -delete"%CAS_FILES)
        return True
    except:
        return False

def main():
    """
    client uploads CAS data files to IBM s3 cloud
    """
    # ARGPARSE code
    parser = argparse.ArgumentParser(description = "A small script to systematically upload/download files from s3")
    parser.add_argument('file', help="Takes a valid configuration.json file which contains credentials to connect to s3")
    args   = parser.parse_args()
  
    # Config File
    config_file = open(args.file)
    getconfig   = json.load(config_file)
  
    trackdateBucket   = "prod-prediction-data-trackdate"
    processdateBucket = "prod-prediction-data-processdate"

    # Get from configuration file
    AWS_ACCESS_KEY_ID     = getconfig["AWS_ACCESS_KEY_ID"]
    S3_HOST               = getconfig["S3_HOST"]
    END_POINT             = getconfig["END_POINT"]
    AWS_ACCESS_KEY_ID     = getconfig["AWS_ACCESS_KEY_ID"]
    AWS_SECRET_ACCESS_KEY = getconfig["AWS_SECRET_ACCESS_KEY"]
    CAS_FILES             = getconfig["CAS_FILES"]

    # S3 connection details
    s3 = boto.s3.connect_to_region(END_POINT,
                            aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                            host=S3_HOST)

    trackdates3Bucket   = s3.get_bucket(trackdateBucket)
    processdates3Bucket = s3.get_bucket(processdateBucket)
    trackdateKey        = Key(trackdates3Bucket)
    processdateKey      = Key(processdates3Bucket)

    # Remove old directories with NO files in them
    if removeEmptyDirs(CAS_FILES):
        LOG.info("Deleted directory successfully in %s"%CAS_FILES)
        return True


    for root, dirnames, filenames in os.walk(CAS_FILES):
        for filename in fnmatch.filter(filenames, "*.csv"):
            filePath = root+"/"+filename
            if compareDate(filePath):
                trackdateFilePath      = s3Path(filePath, trackdateBucket, "TRACKDATE")
                predictiondateFilePath = s3Path(filePath, processdateBucket, "PREDICTIONDATE")
                # Upload to tracedateBucket
                trackdateKey.key   = trackdateFilePath
                processdateKey.key = predictiondateFilePath

                # Upload Files
                if not safeUpload(trackdateKey, filePath, LOG):
                    LOG.warn("Failed uploading %s"%trackdateBucket)
                if not safeUpload(processdateKey, filePath, LOG, deleteFilesToo=True):
                    LOG.warn("Failed uploading %s"%processdateBucket)


if __name__ == "__main__":
    main()
