import argparse
import boto
import datetime
import fcntl
import fnmatch
import logging as LOG
import json

import os
import time
import socket
import struct
import traceback

from boto.s3.key import Key

LOG.basicConfig(format='%(asctime)s  %(levelname)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='clientDownloadScript.log', filemode='a', level=LOG.INFO)

def getAllItems(bucketObject, ROOT_DIR, prefix=""):
    """
    Given a bucketObject and a prefix, download all objects from that prefix
    A blank PREFIX will download the entire bucket
    """
    for key in bucketObject.list(prefix):
        directoryName = ROOT_DIR + "/" + key.name.rsplit("/", 1)[0]
        # Make directory structure from
        if not os.path.exists(directoryName):
            os.makedirs(directoryName)

        key.get_contents_to_filename(ROOT_DIR + "/" + key.name)
        LOG.info("Successfully downloaded %s"%key.name)


def main():
    """
    client uploads CAS data files to IBM s3 cloud
    """
    # ARGPARSE code
    parser = argparse.ArgumentParser(description = "A small script to systematically upload/download files from s3")
    parser.add_argument('file', help="Takes a valid configuration.json file which contains credentials to connect to s3")
    args   = parser.parse_args()
  
    # Config File
    config_file = open(args.file)
    getconfig   = json.load(config_file)
  
    # Get from configuration file
    AWS_ACCESS_KEY_ID     = getconfig["AWS_ACCESS_KEY_ID"]
    S3_HOST               = getconfig["S3_HOST"]
    END_POINT             = getconfig["END_POINT"]
    AWS_ACCESS_KEY_ID     = getconfig["AWS_ACCESS_KEY_ID"]
    AWS_SECRET_ACCESS_KEY = getconfig["AWS_SECRET_ACCESS_KEY"]
    ROOT_DIR              = getconfig["ROOT_DIR"]
    # XXX Same PREFIX will be sent to all buckets
    S3_PREFIX             = getconfig["S3_PREFIX"]
    BUCKET_NAMES          = getconfig["BUCKET_NAMES"]

    # S3 connection details
    s3 = boto.s3.connect_to_region(END_POINT,
                            aws_access_key_id=AWS_ACCESS_KEY_ID,
                            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                            host=S3_HOST)

    bucketObjects       = [s3.get_bucket(x) for x in BUCKET_NAMES]
    for bucketObject in bucketObjects:
        getAllItems(bucketObject, ROOT_DIR, S3_PREFIX)



if __name__ == "__main__":
    main()
